using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndCoins : MonoBehaviour
{
    [SerializeField] private CharacterMovement movement;
    private GUIStyle style = new GUIStyle();
    private void OnGUI()
    {
        style.fontSize = 25;
        style.fontStyle = FontStyle.Normal;

        GUILayout.Label("Coins: " + movement.coins, style);
    }
}
