using UnityEngine;

public class CoinScript : MonoBehaviour
{
    public void OnTriggerEnter(Collider other)
    {
        
        //get tag, not name, because we can have more characters
        if (other.tag == "Player")
        {
            CharacterMovement c = other.GetComponent<CharacterMovement>();
            c.coins++;
            PlayerPrefs.SetInt("coins", c.coins);
            Destroy(gameObject);
        }
        
    }
}
