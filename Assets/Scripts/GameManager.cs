using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    bool gameHasEnded = false;

    public GameObject completeLevelUI;

    public float restartDelay = 0.1f;
   public void GameOver()
    {
        if (!gameHasEnded)
        {
            gameHasEnded = true;
          //  Debug.Log("Game over");
            Invoke("Restart", restartDelay);
        }
    }

    void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        PlayerPrefs.SetInt("coins", 0);
    }

    public void CompleteLevel()
    {
        completeLevelUI.SetActive(true);
    }
}
