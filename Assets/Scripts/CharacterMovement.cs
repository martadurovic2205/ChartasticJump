using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private bool isGrounded;
    [SerializeField] private float groundCheckDistance;
    [SerializeField] private LayerMask groundMask;
    [SerializeField] private float gravity;
    [SerializeField] private Rigidbody rb;
    //for jump
    [SerializeField] private float jumpHeight;

    private Vector3 moveDirection;
    private Vector3 velocity;

    private CharacterController controller;
    private Animator anim;

    public int coins = 0;


    private void Start()
    {
        controller = GetComponent<CharacterController>();
        anim = GetComponent<Animator>();
        coins = PlayerPrefs.GetInt("coins"); //to save number of coins between scenes
    }


    //call for each frame
    private void Update()
    {
        Move();

        if (rb.position.y<-1f)
        {
            FindObjectOfType<GameManager>().GameOver();
        }
    }

    private void Move()
    {
        isGrounded = Physics.CheckSphere(transform.position, groundCheckDistance, groundMask);

        //if character is on ground stop apply gravity
        if(isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }
        //up and down - s=-1,w=1 buttons
        float moveZ = Input.GetAxis("Vertical");
        float moveX = Input.GetAxis("Horizontal");
        moveDirection = new Vector3(moveX, 0, moveZ);
        moveDirection.Normalize();
        transform.Translate(moveDirection * speed * Time.deltaTime, Space.World);

        if (isGrounded)
        {

            if(moveDirection != Vector3.zero)
            {
                transform.forward = moveDirection;
            }
            if (moveDirection == Vector3.zero)
            {
                Idle();
            }
            else
            {
                //run
                moveDirection *= speed;
                anim.SetFloat("speed", 1, 1f, Time.deltaTime);
            }

            if (Input.GetKeyDown(KeyCode.Space))
            {
                Jump();
            }
        }

        //for same amount
        controller.Move(moveDirection*Time.deltaTime);

        //calculate and apply gravity to character
        velocity.y += gravity * Time.deltaTime;
        controller.Move(velocity * Time.deltaTime);

    }

    private void Idle()
    {
        anim.SetFloat("speed", 0, 0.1f, Time.deltaTime);
    }

    private void Jump()
    {
        velocity.y = Mathf.Sqrt(jumpHeight * -2 * gravity);
    }

    private GUIStyle style = new GUIStyle();
    private void OnGUI()
    {
        style.fontSize = 25;
        style.fontStyle = FontStyle.Normal;
        
        GUILayout.Label("Coins: " + coins, style);
    }



}
